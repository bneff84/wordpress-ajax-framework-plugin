/**
 * Add the ajaxRequest function to jQuery
 */
( function($) { 
	
	/*
	 * @param object data The encoded ajaxResponse object data from the server's AJAX framework.
	 */
	$.ajaxResponse = function( data ) {
		return new $.ajaxResponse.prototype.init( data );
	}
	
	/*
	 * Build ajaxResponse prototype
	 */
	$.ajaxResponse.prototype = {
		/*
		 * Status codes that can be returned by the framework.
		 */
		STATUS_OK: 0,
		STATUS_ERROR: 1,
		/*
		 * @param object Data The encoded ajaxResponse object data from the server's AJAX framework.
		 * @return object An instance of the $.ajaxResponse object for this response.
		 */
		init: function( data ) {
			this._data = data;
			return this;
		},
		/*
		 * @param integer checkStatus The status to match against.
		 * @return boolean True if the status matches the passed one, false otherwise.
		 */
		statusIs: function( checkStatus ) {
			return this._data.status == checkStatus;
		},
		/*
		 * @param string key The key of the variable you wish to load.
		 * @param ambiguous defaultValue The default value to return if the key doesn't exist.
		 * @return ambiguous The value of the variable found at key, or the default value if the variable is not defined.
		 */
		getVar: function( key , defaultValue ) {
			if( defaultValue == undefined ) defaultValue = false;
			return this._data.response[ key ] != undefined ? this._data.response[ key ] : defaultValue;
		},
		
		getErrors: function() {
			return this._data.errors;
		},
		
		getError: function( i ) {
			return this._data.errors[ i ] ? this._data.errors[ i ] : false;
		},
		
		getRedirectUrl: function() {
			return this._data.redirectUrl;
		},
		
		doRedirect: function() {
			if( this._data.redirectUrl ) window.location = this._data.redirectUrl;
		},
	}
	
	/*
	 * Alias the prototype onto the init.
	 */
	$.ajaxResponse.prototype.init.prototype = $.ajaxResponse.prototype;
	
	/*
	 * @param string action The action you want to call on the server.
	 * @param object data The data to pass along with the request.
	 * @param function success A callback function which will run on a successful response from the AJAX framework. Should have the signature function( ajaxResponse )
	 * @param function error A callback function which will receive the direct response from jQuery.ajax's error handler. Should have the signature function( jqXHR , textStatus , errorThrown )
	 */
	$.ajaxRequest = function( action , data , success , error ) {
		if( !data ) data = {};
		jQuery.ajax( '/' , {
			headers: { 'X-Requested-With':'WP Ajax Framework' },
			type: 'POST',
			data: {
				ajax: {
					action: action,
					data:data,
				}
			},
			success: function( data ) {
				if( success ) success( data );
			},
			error: function( jqXHR , textStatus , errorThrown ) {
				if( error ) error(  jqXHR , textStatus , errorThrown );
			}
		} );
	}
	
})( jQuery )