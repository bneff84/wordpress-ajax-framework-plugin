# Wordpress AJAX Framework #

A simple programmatic interface for making AJAX requests/responses inside of Wordpress. Particularly useful when you wish to enable rich AJAX functionality in the front-end of the site.

## How to use ##

Using the plugin is simple. Install it, enable it, set up your ajax functions.

```
#!php
//create a function to handle this ajax event
function test_function( &$response , &$data ) {
    if( $data['echo'] ) $response->setVar( 'echo' , $data['echo'] );
}

//bind the event using add_action. Make sure you specify the last parameter as 2 or you won't receive the right number of variables
add_action( 'wp_ajax_framework_action_test' , 'test_function' , 1 , 2 );
```

From there, go to the front-end (or admin panel) of your site, and write the Javascript portion.

```
#!javascript
//execute an AJAX request against the framework, sending along the echo variable which will be returned by the server side function
jQuery.ajaxRequest( 'test' , {echo:'Hello world!'} , function( data ) {
	var response = jQuery.ajaxResponse( data );
	if( response.statusIs( response.STATUS_OK ) ) {
		alert( response.getVar('echo') );
	}
});
```

Run your Javascript code, and you should see an AJAX request fire to the server. When it comes back, you should see an alert that says "Hello world!".

From here, you can build your own AJAX functions into Wordpress using PHP and easily call them from your front-end development using Javascript to get back a structured result.

## Advanced Usage ##

Users who are more advanced, can also extend the prototype for the jQuery.ajaxResponse object onto their own object. Then in your callback function, you can instance your own object instead of the stock jQuery.ajaxResponse one. This can enable you to build more complex objects around the framework.

On the server side, the $response variable is a reference, so overwriting it with your own custom object (which extends the WP_Ajax_Framework_Response object) will allow you to implement the server side extension.