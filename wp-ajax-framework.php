<?php
/*
 Plugin Name: Wordpress Ajax Framework
 Plugin URI: http://wordpress.org/plugins/wp-ajax-framework/
 Description: This plugin adds a programmatic AJAX framework to Wordpress for use in rich application development.
 Author: Brian Neff
 Version: 1.0.0
 Author URI: http://vius.co/
 */

class WP_Ajax_Framework {

    const VERSION = '1.0.0';

    public static function init() {
        //add js side
        wp_enqueue_script('wp-ajax-framework',plugins_url('js/wp-ajax-framework.js',__FILE__),array('jquery'),'1.0.0',true);
        //see if this was an ajaxRequest
        if( self::_isAjaxRequest() ) {
            //instance the WP_Ajax_Framework_Response object
            $response = new WP_Ajax_Framework_Response();
            //get the action
            $action = $_REQUEST['ajax']['action'];
            //get the data
            $data = $_REQUEST['ajax']['data'] ? $_REQUEST['ajax']['data'] : array();
            //run the ajax action if there was one
            if( $action ) do_action_ref_array( 'wp_ajax_framework_action_'.$action , array( &$response , &$data ) );
            //if we're here, it means none of the handlers returned data and exited.
            
            //send headers
            $response->sendContentTypeHeader();
            //output the response
            echo $response;
            exit;
        }
    }

    protected static function _isAjaxRequest() {
        //look for the header for ajax requests sent through the framework
        if( !isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) || $_SERVER['HTTP_X_REQUESTED_WITH'] != 'WP Ajax Framework' ) return false;
        return true;
    }

}

class WP_Ajax_Framework_Response {
    
    const STATUS_OK = 0;
    const STATUS_ERROR = 1;
    
    protected $_status = self::STATUS_OK;
    protected $_errors = array();
    protected $_response = array();
    protected $_redirectUrl = false;
    
    protected $_contentType = 'application/json';
    protected $_charset = 'UTF-8';
    protected $_contentTypeHeaderSent = false;
    
    /**
     * @param string $action The action to execute for this Ajax request.
     */
    public function __construct() {
        //nothing
    }
    
    /**
     * @param string $url The url to redirect to. Does not set the status to redirect automatically. This must be done with _setStatus
     */
    public function setRedirectUrl( $url ) {
        $this->_redirectUrl = $url;
    }
    
    /**
     * @param mixed <number,string> $key The key to set.
     * @param mixed $value The value to set for that key.
     */
    public function setVar( $key , $value ) {
        $this->_response[ $key ] = $value;
    }
    
    /**
     * @param string $key The key to unset.
     */
    public function unsetVar( $key ) {
        unset( $this->_response[ $key ] );
    }
    
    /**
     * @param string $message Adds this error message to the output of the Ajax response. Does not set the status to error automatically. This must be done with _setStatus
     */
    public function addError( $message ) {
        $this->_errors[] = $message;
    }
    
    /**
     * @param number $status The status to set for this Ajax request. Should be one of the AbdltAjaxActions::STATUS_* constants.
     */
    public function setStatus( $status ) {
        $this->_status = $status;
    }
    
    /**
     * @param string $mime The mime content type to output. Default: application/json
     */
    public function setContentType( $mime ) {
        $this->_contentType = $mime;
    }
    
    /**
     * @param string $charset The charset to use for the content type. Default: UTF-8
     */
    public function setCharset( $charset ) {
        $this->_charset = $charset;
    }
    
    /**
     * Outputs the Content-type header with appropriate charset. Can only be called once.
     */
    public function sendContentTypeHeader() {
        //prevent this from running more than once
        if( $this->_contentTypeHeaderSent ) return;
        @header( 'Content-Type: '.$this->_contentType.'; charset=' . $this->_charset );
        $this->_contentTypeHeaderSent = true;
    }
    
    /*
     * Magic class methods
     */
    
    public function __toString() {
        $response = array(
            'status' => $this->_status
        );
        //only output errors if we have some
        if( !empty( $this->_errors ) ) {
            $response['errors'] = $this->_errors;
        }
        //only output response variables if we have some
        if( !empty( $this->_response ) ) {
            $response['response'] = $this->_response;
        }
        //only output redirect url if we have one
        if( !empty( $this->_redirectUrl ) ) {
            $response['redirectUrl'] = $this->_redirectUrl;
        }
        return json_encode( $response );
    }
    
}

//hook the init at 999 so we're one of the last plugins to run
add_action( 'init' , array( 'WP_Ajax_Framework' , 'init' ) , 999 );